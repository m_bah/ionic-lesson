import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalController, MenuController, NavController, ToastController, LoadingController } from 'ionic-angular';
import { SingleAppareilPage } from './single-appareil/single-appareil';
import { Appareil } from '../../models/appareil';
import { AppareilsService } from '../../services/appareils.service';
import { AppareilFormPage } from './appareil-form/appareil-form';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'page-appareils',
  templateUrl: 'appareils.html'
})
export class AppareilsPage implements OnInit, OnDestroy {

    appareilsList: Appareil[];
    appareilsSubscription: Subscription;

    constructor(private modalCtrl: ModalController, 
        private appareilService: AppareilsService, 
        private navCtrl: NavController,
        private menuCtrl: MenuController,
        private toastCtrl: ToastController,
        private loadingCtrl: LoadingController) {}

    ngOnInit() {
        this.appareilsSubscription = this.appareilService.appareil$.subscribe(
            (appareils: Appareil[]) => {
                this.appareilsList = appareils.slice();
            }
        );
        this.appareilService.emitAppareil();
    }

    ionViewWillEnter() {
        this.appareilsList = this.appareilService.appareilsList.slice();
    }

    onLoadAppareil(index: number) {
        //this.navCtrl.push(SingleAppareilPage, {appareil: appareil});
        let modal = this.modalCtrl.create(SingleAppareilPage, {index: index});
        modal.present();
    }
    
    onToggleMenu() {
     this.menuCtrl.open();
    }
    onNewAppareil() {
        this.navCtrl.push(AppareilFormPage);
    }

    ngOnDestroy() {
        this.appareilsSubscription.unsubscribe();
    }

    /**
     * save data
     */
    onSaveList() {
        let loader = this.loadingCtrl.create({
            content: 'Sauvegarde en cours...'
        });
        loader.present();
        this.appareilService.saveData().then(
            () => {
                loader.dismiss();
                this.toastCtrl.create({
                    message: 'Données sauvegardées !',
                    duration: 3000,
                    position: 'top'
                }).present();
            },
            (error) => {
                loader.dismiss();
                this.toastCtrl.create({
                    message: error,
                    duration: 3000,
                    position: 'top'
                }).present();
            }
        );
    }

    /**
     * Fetch data
     */
    onFetchList() {
        let loader = this.loadingCtrl.create({
            content: 'Récuperation en cours'
        });
        loader.present();
        this.appareilService.retrieveData().then(
            () => {
                loader.dismiss();
                this.toastCtrl.create({
                    message: 'Données récupérées !',
                    duration: 3000,
                    position: 'top'
                }).present();
            },
            (error) => {
                loader.dismiss();
                this.toastCtrl.create({
                    message: error,
                    duration: 3000,
                    position: 'top'
                }).present();
            }
        )
    }
}