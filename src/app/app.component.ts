import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import * as firebase from 'firebase';

import { TabsPage } from '../pages/tabs/tabs';
import { OptionPage } from '../pages/option/option';
import { AuthPage } from '../pages/auth/auth';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  tabsPage:any = TabsPage;
  optionsPage:any = OptionPage;
  authPage:any = AuthPage;
  isAuth = false;
  @ViewChild('content') content: NavController;
  constructor(platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private menuCtrl: MenuController) {
    platform.ready().then(() => {
    statusBar.styleDefault();
    splashScreen.hide();
    let config = {
      apiKey: "AIzaSyAddVtgcPSGJBlPDF-Bys8sSLDMkWcC_kI",
      authDomain: "oc-ionic-84ee8.firebaseapp.com",
      databaseURL: "https://oc-ionic-84ee8.firebaseio.com",
      projectId: "oc-ionic-84ee8",
      storageBucket: "oc-ionic-84ee8.appspot.com",
      messagingSenderId: "89268691592"
    };
      firebase.initializeApp(config);
      firebase.auth().onAuthStateChanged(
        (user) => {
          if (user) { 
          this.isAuth = true;
            this.content.setRoot(TabsPage);
          } else {
            this.isAuth = false;
            this.content.setRoot(AuthPage, {mode: 'connect'});
          }
        }
      )
    });
  }

  onNavigate(page:any, data?:{}) {
    this.content.setRoot(page, data? data : null);
    this.menuCtrl.close();
  }

  onDisconnect() {
    firebase.auth().signOut();
    this.menuCtrl.close();
  }
}

