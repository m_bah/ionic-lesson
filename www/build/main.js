webpackJsonp([0],{

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__appareils_appareils__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings_settings__ = __webpack_require__(203);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TabsPage = /** @class */ (function () {
    function TabsPage() {
        this.appareilsPage = __WEBPACK_IMPORTED_MODULE_1__appareils_appareils__["a" /* AppareilsPage */];
        this.settingsPage = __WEBPACK_IMPORTED_MODULE_2__settings_settings__["a" /* SettingsPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tabs',template:/*ion-inline-start:"/home/mbah/Angular/Ionic/projet-ionic/src/pages/tabs/tabs.html"*/'<ion-tabs>\n    <ion-tab [root]="appareilsPage" tabTitle="Appareils" tabIcon="bulb"></ion-tab>\n    <ion-tab [root]="settingsPage" tabTitle="Settings" tabIcon="settings"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"/home/mbah/Angular/Ionic/projet-ionic/src/pages/tabs/tabs.html"*/
        })
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppareilsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__single_appareil_single_appareil__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_appareils_service__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__appareil_form_appareil_form__ = __webpack_require__(202);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AppareilsPage = /** @class */ (function () {
    function AppareilsPage(modalCtrl, appareilService, navCtrl, menuCtrl, toastCtrl, loadingCtrl) {
        this.modalCtrl = modalCtrl;
        this.appareilService = appareilService;
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
    }
    AppareilsPage.prototype.ngOnInit = function () {
        var _this = this;
        this.appareilsSubscription = this.appareilService.appareil$.subscribe(function (appareils) {
            _this.appareilsList = appareils.slice();
        });
        this.appareilService.emitAppareil();
    };
    AppareilsPage.prototype.ionViewWillEnter = function () {
        this.appareilsList = this.appareilService.appareilsList.slice();
    };
    AppareilsPage.prototype.onLoadAppareil = function (index) {
        //this.navCtrl.push(SingleAppareilPage, {appareil: appareil});
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__single_appareil_single_appareil__["a" /* SingleAppareilPage */], { index: index });
        modal.present();
    };
    AppareilsPage.prototype.onToggleMenu = function () {
        this.menuCtrl.open();
    };
    AppareilsPage.prototype.onNewAppareil = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__appareil_form_appareil_form__["a" /* AppareilFormPage */]);
    };
    AppareilsPage.prototype.ngOnDestroy = function () {
        this.appareilsSubscription.unsubscribe();
    };
    /**
     * save data
     */
    AppareilsPage.prototype.onSaveList = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: 'Sauvegarde en cours...'
        });
        loader.present();
        this.appareilService.saveData().then(function () {
            loader.dismiss();
            _this.toastCtrl.create({
                message: 'Données sauvegardées !',
                duration: 3000,
                position: 'top'
            }).present();
        }, function (error) {
            loader.dismiss();
            _this.toastCtrl.create({
                message: error,
                duration: 3000,
                position: 'top'
            }).present();
        });
    };
    /**
     * Fetch data
     */
    AppareilsPage.prototype.onFetchList = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: 'Récuperation en cours'
        });
        loader.present();
        this.appareilService.retrieveData().then(function () {
            loader.dismiss();
            _this.toastCtrl.create({
                message: 'Données récupérées !',
                duration: 3000,
                position: 'top'
            }).present();
        }, function (error) {
            loader.dismiss();
            _this.toastCtrl.create({
                message: error,
                duration: 3000,
                position: 'top'
            }).present();
        });
    };
    AppareilsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-appareils',template:/*ion-inline-start:"/home/mbah/Angular/Ionic/projet-ionic/src/pages/appareils/appareils.html"*/'<ion-header>\n    <ion-navbar>\n      <ion-buttons start>\n        <button ion-button icon-only (click)="onToggleMenu()">\n          <ion-icon name="menu"></ion-icon>\n        </button>\n      </ion-buttons>\n      <ion-title>Appareils</ion-title>\n    </ion-navbar>\n  </ion-header>\n  \n  <ion-content padding>\n    <ion-list>\n      <ion-item>\n          <button icon-start ion-item *ngFor="let appareil of appareilsList; let i = index" \n          (click)="onLoadAppareil(i)">\n          <ion-icon name="md-power" [color]="appareil.isOn ? \'secondary\' : \'danger\'"></ion-icon>\n            {{appareil.name}}\n          </button>\n      </ion-item>\n    </ion-list>\n    <button ion-button round full (click)="onNewAppareil()">Nouvel appareil</button>\n  \n  <ion-card>\n    <ion-card-header>Données</ion-card-header>\n     <ion-card-content>\n        <button ion-button block outline (click)="onSaveList()">Enregistrer</button>\n        <button ion-button block outline (click)="onFetchList()">Récupérer</button>\n     </ion-card-content>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/home/mbah/Angular/Ionic/projet-ionic/src/pages/appareils/appareils.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services_appareils_service__["a" /* AppareilsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_appareils_service__["a" /* AppareilsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* MenuController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* MenuController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]) === "function" && _f || Object])
    ], AppareilsPage);
    return AppareilsPage;
    var _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=appareils.js.map

/***/ }),

/***/ 115:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 115;

/***/ }),

/***/ 156:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 156;

/***/ }),

/***/ 201:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SingleAppareilPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_appareils_service__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SingleAppareilPage = /** @class */ (function () {
    function SingleAppareilPage(navParams, viewCtrl, appareilsService) {
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.appareilsService = appareilsService;
    }
    SingleAppareilPage.prototype.ngOnInit = function () {
        this.index = this.navParams.get('index');
        this.appareil = this.appareilsService.appareilsList[this.index];
    };
    SingleAppareilPage.prototype.dismissModal = function () {
        this.viewCtrl.dismiss();
    };
    SingleAppareilPage.prototype.onToggleAppareil = function () {
        this.appareil.isOn = !this.appareil.isOn;
    };
    SingleAppareilPage.prototype.onSubmitForm = function (form) {
        console.log(form.value);
        this.dismissModal();
    };
    SingleAppareilPage.prototype.onDeleteHours = function () {
        this.appareil.startTime = '';
        this.appareil.endTime = '';
        this.dismissModal();
    };
    SingleAppareilPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-single-appareil',template:/*ion-inline-start:"/home/mbah/Angular/Ionic/projet-ionic/src/pages/appareils/single-appareil/single-appareil.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons>\n      <button ion-button clear (click)="dismissModal()">Fermer</button>\n    </ion-buttons>\n    <ion-title>{{ appareil.name }}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n  <ion-card [ngClass]="{\'is-on\': appareil.isOn, \'is-off\': !appareil.isOn}">\n    <ion-card-header>{{ appareil.name }}</ion-card-header>\n    <ion-card-content>\n      <p *ngFor="let line of appareil.description">{{ line }}</p>\n    </ion-card-content>\n    \n    <ion-grid>\n      <ion-row>\n        <ion-col>\n            <button\n            ion-button\n            full\n            color="danger"\n            [disabled]="!appareil.isOn"\n            (click)="onToggleAppareil()">Eteindre</button>\n        </ion-col>\n        <ion-col>\n          <button\n            ion-button\n            full\n            color="secondary"\n            [disabled]="appareil.isOn"\n            (click)="onToggleAppareil()">Allumer</button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n    <form #f="ngForm" (ngSubmit)="onSubmitForm(f)">\n      <ion-list>\n        <ion-item>\n          <ion-label>Heure d\'allumage</ion-label>\n          <ion-datetime displayFormat="HH:mm" name="startTime" [(ngModel)]="appareil.startTime"></ion-datetime>\n        </ion-item>\n        <ion-item>\n          <ion-label>Heure d\'extinction</ion-label>\n          <ion-datetime displayFormat="HH:mm" name="endTime" [(ngModel)]="appareil.endTime"></ion-datetime>\n        </ion-item>\n      </ion-list>\n      <button type="button"\n            ion-button\n            full\n            color="danger"\n            (click)="onDeleteHours()">\n      Supprimer horaires\n      </button>\n      <button type="submit"\n            ion-button\n            full\n            color="secondary">\n      Enregistrer horaires\n      </button>\n    </form>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/home/mbah/Angular/Ionic/projet-ionic/src/pages/appareils/single-appareil/single-appareil.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__services_appareils_service__["a" /* AppareilsService */]])
    ], SingleAppareilPage);
    return SingleAppareilPage;
}());

//# sourceMappingURL=single-appareil.js.map

/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppareilFormPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_appareil__ = __webpack_require__(292);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_appareils_service__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AppareilFormPage = /** @class */ (function () {
    function AppareilFormPage(formBuilder, navCtrl, appareilsService) {
        this.formBuilder = formBuilder;
        this.navCtrl = navCtrl;
        this.appareilsService = appareilsService;
    }
    AppareilFormPage.prototype.ngOnInit = function () {
        this.initForm();
    };
    AppareilFormPage.prototype.initForm = function () {
        this.appareilForm = this.formBuilder.group({
            name: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
            description: this.formBuilder.array([])
        });
    };
    AppareilFormPage.prototype.getDescriptionArray = function () {
        return this.appareilForm.get('description');
    };
    AppareilFormPage.prototype.onAddDescription = function () {
        var newControl = this.formBuilder.control('');
        this.getDescriptionArray().controls.push(newControl);
    };
    AppareilFormPage.prototype.onRemoveDescription = function (index) {
        this.getDescriptionArray().removeAt(index);
    };
    AppareilFormPage.prototype.onSubmitForm = function () {
        var newAppareil = new __WEBPACK_IMPORTED_MODULE_2__models_appareil__["a" /* Appareil */](this.appareilForm.get('name').value);
        for (var _i = 0, _a = this.getDescriptionArray().controls; _i < _a.length; _i++) {
            var control = _a[_i];
            newAppareil.description.push(control.value);
        }
        this.appareilsService.addAppareil(newAppareil);
        this.navCtrl.pop();
    };
    AppareilFormPage.prototype.isNotNull = function () {
        return (this.appareilForm.get('name').value.length > 0) ? false : true;
    };
    AppareilFormPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-appareil-form',template:/*ion-inline-start:"/home/mbah/Angular/Ionic/projet-ionic/src/pages/appareils/appareil-form/appareil-form.html"*/'<ion-header>\n    <ion-navbar>\n      <ion-title>Nouvel appareil</ion-title>\n    </ion-navbar>\n  </ion-header>\n  \n  <ion-content padding>\n    <form [formGroup]="appareilForm">\n      <ion-list>\n        <ion-item-divider>Informations</ion-item-divider>\n        <ion-item>\n          <ion-label>Nom : </ion-label>\n          <ion-input placeholder="Appareil" formControlName="name"></ion-input>\n        </ion-item>\n        <ion-item-divider>Description</ion-item-divider>\n        <div formArrayName="description">\n          <ion-item *ngFor="let control of getDescriptionArray().controls; let i = index">\n            <ion-icon item-end\n                    name="remove-circle"\n                    color="danger"\n                    (click)="onRemoveDescription(i)">\n            </ion-icon>  \n            <ion-input [formControlName]="i" placeholder="{{i+1}}"></ion-input>\n          </ion-item>\n        </div>\n      </ion-list>\n      <button ion-button round full (click)="onAddDescription()">Ajouter une ligne {{disableButton}}</button>\n      <button ion-button full round color="secondary"  [disabled]="isNotNull()" (input)="isNotNull()" (click)="onSubmitForm()">Enregistrer</button>\n    </form>\n  </ion-content>'/*ion-inline-end:"/home/mbah/Angular/Ionic/projet-ionic/src/pages/appareils/appareil-form/appareil-form.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__services_appareils_service__["a" /* AppareilsService */]])
    ], AppareilFormPage);
    return AppareilFormPage;
}());

//# sourceMappingURL=appareil-form.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SettingsPage = /** @class */ (function () {
    function SettingsPage(alertCtrl, menuCtrl) {
        this.alertCtrl = alertCtrl;
        this.menuCtrl = menuCtrl;
    }
    SettingsPage.prototype.onToggleLights = function () {
        var alert = this.alertCtrl.create({
            title: 'Êtes vous certain(e) de voulloir continuer ?',
            subTitle: 'Cette action allumera ou étteindra toutes les lumières de la maison',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel'
                },
                {
                    text: 'Confirmer',
                    handler: function () { return console.log('Confimé !'); }
                }
            ]
        });
        alert.present();
    };
    SettingsPage.prototype.onToggleMenu = function () {
        this.menuCtrl.open();
    };
    SettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-settings',template:/*ion-inline-start:"/home/mbah/Angular/Ionic/projet-ionic/src/pages/settings/settings.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons start>\n        <button ion-button icon-only (click)="onToggleMenu()">\n          <ion-icon name="menu"></ion-icon>\n        </button>\n    </ion-buttons>\n    <ion-title>Réglages</ion-title>\n  </ion-navbar>\n</ion-header>\n  \n<ion-content padding>\n  <button ion-button icon-start (click)="onToggleLights()">\n    <ion-icon name="power"></ion-icon>\n    Lumières\n  </button>\n</ion-content>'/*ion-inline-end:"/home/mbah/Angular/Ionic/projet-ionic/src/pages/settings/settings.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* MenuController */]])
    ], SettingsPage);
    return SettingsPage;
}());

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OptionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var OptionPage = /** @class */ (function () {
    function OptionPage(menuCtrl) {
        this.menuCtrl = menuCtrl;
    }
    OptionPage.prototype.onToggleMenu = function () {
        this.menuCtrl.open();
    };
    OptionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-option',template:/*ion-inline-start:"/home/mbah/Angular/Ionic/projet-ionic/src/pages/option/option.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-buttons start>\n            <button ion-button icon-only (click)="onToggleMenu()">\n                <ion-icon name="menu"></ion-icon>\n            </button>\n        </ion-buttons>\n        <ion-title>Options</ion-title>\n    </ion-navbar>\n</ion-header>\n <ion-content padding>\n     <p>Modifiez les options de l\'application ici !</p>\n</ion-content>'/*ion-inline-end:"/home/mbah/Angular/Ionic/projet-ionic/src/pages/option/option.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* MenuController */]])
    ], OptionPage);
    return OptionPage;
}());

//# sourceMappingURL=option.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_auth_service__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabs_tabs__ = __webpack_require__(105);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AuthPage = /** @class */ (function () {
    function AuthPage(authService, navParams, menuCtrl, navCtrl, formBuilder) {
        this.authService = authService;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
    }
    AuthPage.prototype.ngOnInit = function () {
        this.mode = this.navParams.get('mode');
        this.initForm();
    };
    AuthPage.prototype.onToggleMenu = function () {
        this.menuCtrl.open();
    };
    AuthPage.prototype.initForm = function () {
        this.authForm = this.formBuilder.group({
            email: ['', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].email]],
            password: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required]
        });
    };
    AuthPage.prototype.onSubmitForm = function () {
        var _this = this;
        var email = this.authForm.get('email').value;
        var password = this.authForm.get('password').value;
        if (this.mode === 'new') {
            this.authService.signUpUser(email, password).then(function () {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__tabs_tabs__["a" /* TabsPage */]);
            }, function (error) {
                _this.errorMessage = error;
            });
        }
        else if (this.mode === 'connect') {
            this.authService.signInUser(email, password).then(function () {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__tabs_tabs__["a" /* TabsPage */]);
            }, function (error) {
                _this.errorMessage = error;
            });
        }
    };
    AuthPage.prototype.disableButton = function () {
        return (this.authForm.get('email').value.length > 0 && this.authForm.get('password').value.length > 0) ? false : true;
    };
    AuthPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-auth',template:/*ion-inline-start:"/home/mbah/Angular/Ionic/projet-ionic/src/pages/auth/auth.html"*/'<ion-header>\n    <ion-navbar>\n      <ion-buttons start>\n        <button ion-button icon-only (click)="onToggleMenu()">\n          <ion-icon name="menu"></ion-icon>\n        </button>\n      </ion-buttons>\n      <ion-title *ngIf="mode === \'new\'">Nouvel utilisateur</ion-title>\n      <ion-title *ngIf="mode === \'connect\'">Connexion</ion-title>\n    </ion-navbar>\n</ion-header>\n  \n<ion-content padding>\n<form [formGroup]="authForm">\n    <ion-list>\n        <ion-item>\n            <ion-label floating>Adresse email</ion-label>\n            <ion-input type="email" formControlName="email"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label floating>Mot de passe</ion-label>\n            <ion-input type="password" formControlName="password"></ion-input>\n        </ion-item>\n    </ion-list>\n    <button ion-button round full (click)="onSubmitForm()" [disabled]="disableButton()" (input)="disableButton()">Soumettre</button>\n    <span ion-text color="danger">{{errorMessage}}</span>\n</form>\n</ion-content>'/*ion-inline-end:"/home/mbah/Angular/Ionic/projet-ionic/src/pages/auth/auth.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_auth_service__["a" /* AuthService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* MenuController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* MenuController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */]) === "function" && _e || Object])
    ], AuthPage);
    return AuthPage;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_firebase__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_firebase__);

var AuthService = /** @class */ (function () {
    function AuthService() {
        var _this = this;
        this.isAuth = false;
        __WEBPACK_IMPORTED_MODULE_0_firebase__["auth"]().onAuthStateChanged(function (user) {
            if (user) {
                _this.isAuth = true;
            }
            else {
                _this.isAuth = false;
            }
        });
    }
    AuthService.prototype.signUpUser = function (email, password) {
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_0_firebase__["auth"]().createUserAndRetrieveDataWithEmailAndPassword(email, password).then(function (user) {
                resolve(user);
            }, function (error) {
                reject(error);
            });
        });
    };
    AuthService.prototype.signInUser = function (email, password) {
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_0_firebase__["auth"]().signInWithEmailAndPassword(email, password).then(function (user) {
                resolve(user);
            }, function (error) {
                reject(error);
            });
        });
    };
    AuthService.prototype.signOut = function () {
        __WEBPACK_IMPORTED_MODULE_0_firebase__["auth"]().signOut();
    };
    return AuthService;
}());

//# sourceMappingURL=auth.service.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(230);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 230:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(293);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_appareils_appareils__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_appareils_single_appareil_single_appareil__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_settings_settings__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_appareils_service__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_option_option__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_appareils_appareil_form_appareil_form__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_auth_service__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_auth_auth__ = __webpack_require__(205);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_appareils_appareils__["a" /* AppareilsPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_appareils_single_appareil_single_appareil__["a" /* SingleAppareilPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_option_option__["a" /* OptionPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_auth_auth__["a" /* AuthPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_appareils_appareil_form_appareil_form__["a" /* AppareilFormPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__["a" /* TabsPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: []
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_appareils_appareils__["a" /* AppareilsPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_appareils_single_appareil_single_appareil__["a" /* SingleAppareilPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_appareils_appareil_form_appareil_form__["a" /* AppareilFormPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_option_option__["a" /* OptionPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_auth_auth__["a" /* AuthPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__["a" /* TabsPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_11__services_appareils_service__["a" /* AppareilsService */],
                __WEBPACK_IMPORTED_MODULE_14__services_auth_service__["a" /* AuthService */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 280:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_tabs_tabs__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_option_option__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_auth_auth__ = __webpack_require__(205);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, menuCtrl) {
        var _this = this;
        this.menuCtrl = menuCtrl;
        this.tabsPage = __WEBPACK_IMPORTED_MODULE_5__pages_tabs_tabs__["a" /* TabsPage */];
        this.optionsPage = __WEBPACK_IMPORTED_MODULE_6__pages_option_option__["a" /* OptionPage */];
        this.authPage = __WEBPACK_IMPORTED_MODULE_7__pages_auth_auth__["a" /* AuthPage */];
        this.isAuth = false;
        platform.ready().then(function () {
            statusBar.styleDefault();
            splashScreen.hide();
            var config = {
                apiKey: "AIzaSyAddVtgcPSGJBlPDF-Bys8sSLDMkWcC_kI",
                authDomain: "oc-ionic-84ee8.firebaseapp.com",
                databaseURL: "https://oc-ionic-84ee8.firebaseio.com",
                projectId: "oc-ionic-84ee8",
                storageBucket: "oc-ionic-84ee8.appspot.com",
                messagingSenderId: "89268691592"
            };
            __WEBPACK_IMPORTED_MODULE_4_firebase__["initializeApp"](config);
            __WEBPACK_IMPORTED_MODULE_4_firebase__["auth"]().onAuthStateChanged(function (user) {
                if (user) {
                    _this.isAuth = true;
                    _this.content.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_tabs_tabs__["a" /* TabsPage */]);
                }
                else {
                    _this.isAuth = false;
                    _this.content.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_auth_auth__["a" /* AuthPage */], { mode: 'connect' });
                }
            });
        });
    }
    MyApp.prototype.onNavigate = function (page, data) {
        this.content.setRoot(page, data ? data : null);
        this.menuCtrl.close();
    };
    MyApp.prototype.onDisconnect = function () {
        __WEBPACK_IMPORTED_MODULE_4_firebase__["auth"]().signOut();
        this.menuCtrl.close();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('content'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */])
    ], MyApp.prototype, "content", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/mbah/Angular/Ionic/projet-ionic/src/app/app.html"*/'<ion-menu [content]="content">\n    <ion-header>\n        <ion-toolbar>\n        <ion-title>Menu</ion-title>\n        </ion-toolbar>\n    </ion-header>\n    <ion-content>\n      <ion-list>\n        <button *ngIf="isAuth" ion-item icon-start (click)="onNavigate(tabsPage)">\n            <ion-icon name="power"></ion-icon>\n            Appareils\n        </button>\n        <button *ngIf="isAuth" ion-item icon-start (click)="onNavigate(optionsPage)">\n            <ion-icon name="options"></ion-icon>\n            Options\n        </button>\n        <button *ngIf="!isAuth" ion-item icon-start (click)="onNavigate(authPage, {mode: \'new\'})">\n            <ion-icon name="person-add"></ion-icon>\n            Nouvel utilisateur\n        </button>\n        <button *ngIf="!isAuth" ion-item icon-start (click)="onNavigate(authPage, {mode: \'connect\'})">\n            <ion-icon name="person"></ion-icon>\n            Connexion\n        </button>\n        <button ion-item icon-start (click)="onDisconnect()" *ngIf="isAuth">\n            <ion-icon name="exit"></ion-icon>\n            Déconnexion\n            </button>\n      </ion-list>\n    </ion-content>\n</ion-menu>\n    <ion-nav [root]="tabsPage" #content></ion-nav>'/*ion-inline-end:"/home/mbah/Angular/Ionic/projet-ionic/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* MenuController */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 292:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Appareil; });
var Appareil = /** @class */ (function () {
    function Appareil(name) {
        this.name = name;
        this.description = [];
        this.isOn = false;
        this.startTime = '';
        this.endTime = '';
    }
    return Appareil;
}());

//# sourceMappingURL=appareil.js.map

/***/ }),

/***/ 293:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__appareils_appareils__ = __webpack_require__(106);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var HomePage = /** @class */ (function () {
    function HomePage() {
        this.appareilsPage = __WEBPACK_IMPORTED_MODULE_1__appareils_appareils__["a" /* AppareilsPage */];
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/mbah/Angular/Ionic/projet-ionic/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Page d\'accueil\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <button ion-button [navPush]="appareilsPage" >Appareils</button>\n</ion-content>\n'/*ion-inline-end:"/home/mbah/Angular/Ionic/projet-ionic/src/pages/home/home.html"*/
        })
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppareilsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_Subject__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_firebase__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_firebase__);


var AppareilsService = /** @class */ (function () {
    function AppareilsService() {
        this.appareil$ = new __WEBPACK_IMPORTED_MODULE_0_rxjs_Subject__["Subject"]();
        this.appareilsList = [
            {
                name: 'Machine à laver',
                description: [
                    'Volume: 6 litres',
                    'Temps de lavage: 2 heures',
                    'Consommation: 173 kWh/an'
                ],
                isOn: true,
                startTime: '',
                endTime: ''
            },
            {
                name: 'Télévision',
                description: [
                    'Dimensions: 40 pouces',
                    'Consommation: 22 kWh/an'
                ],
                isOn: true,
                startTime: '',
                endTime: ''
            },
            {
                name: 'Ordinateur',
                description: [
                    'Marque: fait maison',
                    'Consommation: 500 kWh/an'
                ],
                isOn: true,
                startTime: '',
                endTime: ''
            }
        ];
    }
    AppareilsService.prototype.addAppareil = function (appareil) {
        this.appareilsList.push(appareil);
        this.emitAppareil();
    };
    AppareilsService.prototype.emitAppareil = function () {
        this.appareil$.next(this.appareilsList.slice());
    };
    /**
     * Save data into the database
     */
    AppareilsService.prototype.saveData = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_1_firebase__["database"]().ref('appareils').set(_this.appareilsList).then(function (data) {
                resolve(data);
            }, function (error) {
                reject(error);
            });
        });
    };
    AppareilsService.prototype.retrieveData = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_1_firebase__["database"]().ref('appareils').once('value').then(function (data) {
                _this.appareilsList = data.val();
                _this.emitAppareil();
                resolve('Données récupérées avec succès !');
            }, function (error) {
                reject(error);
            });
        });
    };
    return AppareilsService;
}());

//# sourceMappingURL=appareils.service.js.map

/***/ })

},[207]);
//# sourceMappingURL=main.js.map